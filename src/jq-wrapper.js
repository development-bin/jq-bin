#!/usr/bin/env node
'use strict';

const { spawn } = require('child_process');
const { resolve } = require('path');

const execName = process.platform === 'win32' ? 'jq.exe' : './jq';
const command = resolve(__dirname, '..', 'tools', execName);
const jq = spawn(command, process.argv.slice(2), { cwd: process.cwd() });
jq.stdout.pipe(process.stdout);
jq.stderr.pipe(process.stderr);
jq.on('error', function(err) {
  console.error(`Received an error while executing the JQ binary: ${err}`);
  process.exit(1);
});
